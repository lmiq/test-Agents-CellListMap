using Agents, LinearAlgebra, StaticArrays, Random
using InteractiveDynamics, GLMakie
import CellListMap

include("obstacles.jl")

mutable struct CellListMapData{B,C,A,O}
    box::B
    cell_list::C
    aux::A
    output_threaded::O
    cell_list_rcut::Float64
    rods_pos::Vector{SVector{2,Float64}} # To avoid reallocating 
end

@agent ActiveRod ContinuousAgent{2} begin
    l::Float64
    d::Float64
    θ::Float64
    image::SVector{2,Int64}
end
ActiveRod(; l=2.0, d=1.0, θ=0, image=[0, 0]) = ActiveRod(l, d, θ, image)

struct ForceTorque
    force::SVector{2,Float64}
    torque::Float64
end
import Base: copy, zero, +
copy(x::ForceTorque) = x
zero(::Type{ForceTorque}) = ForceTorque(zero(SVector{2,Float64}), 0.0)
+(x::ForceTorque, y::ForceTorque) = ForceTorque(x.force + y.force, x.torque + y.torque)

Base.@kwdef mutable struct Parameters{CLMAP}
    dt::Float64 = 0.01
    va::Float64 = 1.0
    λ0::Float64 = 0.0
    k0::Float64 = 50.0
    γ::Float64 = 1.0
    l::Float64 = 2.0
    d::Float64 = 1.0
    ζ::Vector{Float64}
    n_obs::Int64 = 0
    obs_pos::Vector{SVector{2,Float64}}
    obs_radius::Float64 = 5.0
    force_torque::Vector{ForceTorque}
    rcut::Float64 = 5.5
    rcut2::Float64 = 30.25
    clmap::CLMAP
end

function initialize_model(;
    n_rods=1,
    n_obs=0,
    extent=(50.0, 50.0),
    dt=0.01,
    va=1.0,
    k0=1.0,
    λ0=0.0,
    γ=1.0,
    l=2.0,
    d=1.0,
    obs_radius=5.0,
    cell_list_rcut=10.0,
    seed=12345
)
    rcut = d / 2 + obs_radius # colision distance
    rng = MersenneTwister(seed)
    space2d = ContinuousSpace(extent)
    # random positions of rods and obstacles
    rods_pos = [rand(rng, SVector{2,Float64}) .* extent[1] for _ in 1:n_rods]
    obs_pos = initialize_obstacles(n_obs, extent, rng)
    # rand number for active rods: set to 0.0 initialy.
    ζ = zeros(n_rods)
    # initialize forces and torques
    force_torque = zeros(ForceTorque, n_rods)

    # Data for using cell lists
    box_sides = [extent...]
    box = CellListMap.Box(box_sides, cell_list_rcut)
    if n_obs > 0
        cl = CellListMap.CellList(rods_pos, obs_pos, box)
    else
        cl = CellListMap.CellList(rods_pos, box)
    end
    aux = CellListMap.AuxThreaded(cl)
    output_threaded = [copy(force_torque) for _ in 1:CellListMap.nbatches(cl)]
    clmap = CellListMapData(box, cl, aux, output_threaded, cell_list_rcut, rods_pos)

    # define the model
    properties = Parameters{typeof(clmap)}(dt, va, λ0, k0,
        γ, l, d, ζ,
        n_obs, obs_pos, obs_radius,
        force_torque,
        rcut, rcut^2,
        clmap,
    )
    model = ABM(ActiveRod,
        space2d,
        properties=properties
    )

    # create active rods
    for i in 1:n_rods
        θ = rand() * π
        v = (cos(θ), sin(θ)) .* va
        add_agent!(NTuple{2,Float64}(rods_pos[i]), model, v, l, d, θ, [0, 0])
    end

    return model
end


function agent_step!(agent, model::ABM)
    # get my id
    id = agent.id
    ζ = model.ζ[id]
    λ0 = model.λ0
    τ_tot = model.force_torque[id].torque # Total torque
    f_tot = model.force_torque[id].force # Total force

    n = rad2vec(agent.θ) # direction 

    r_old = agent.pos
    Δr = model.dt .* ((model.va .* n) .+ (f_tot ./ model.γ))
    walk!(agent, Tuple(Δr), model)

    agent.θ = agent.θ + model.dt * τ_tot / model.γ

    # random tumbling
    if ζ < λ0
        θ_rand = rand(model.rng) * 2π
        agent.θ = agent.θ + θ_rand
    end

    # update image
    dr = r_old .- agent.pos
    passed_pbc = abs.(dr) .> model.space.extent[1] / 2
    image = any(passed_pbc) ? Int.(sign.(dr)) .* passed_pbc : (0, 0)
    agent.image = agent.image .+ image
    return agent.pos
end

function calc_force_rod_obs(dr, n, model::ABM)
    k0 = model.k0
    lhalf = model.l / 2
    ci = dot(dr, n)
    ci = abs(ci) > lhalf ? lhalf * sign(ci) : ci
    rij = 1 .* (dr .- (ci .* n))
    rij_mag = euclidean_distance((0.0, 0.0), NTuple{2,Float64}(rij), model)
    #rij_mag = norm(rij)
    dij = model.rcut # colision distance
    #rij_normal = rij_mag > eps() ? rij ./ rij_mag : (0.0, 0.0)
    rij_normal = unit_vec(rij)
    fij = rij_mag <= dij ? k0 .* (dij - rij_mag) .* rij_normal : SVector{2,Float64}(0.0, 0.0)
    yij = (ci .* n) .+ (rij ./ 2) # force arm
    #τij = cross(-1 .* [yij..., 0.0], [fij..., 0.0])[3]
    τij = -yij[1] * fij[2] + yij[2] * fij[1] # yij × fij
    return ForceTorque(fij, τij)
end

function calc_forces_torque!(x, y, i, model, force_torque)
    #if d2 > model.rcut2
    #    return force_torque
    #end
    dr = x - y
    n = SVector{2,Float64}(rad2vec(model[i].θ))#unit_vec(model[i].vel) # orientation
    #D = model.obs_radius
    force_torque[i] = calc_force_rod_obs(dr, n, model)
    return force_torque
end

function model_step!(model::ABM)
    # get the vector of rods positions
    for id in eachindex(model.clmap.rods_pos)
        model.clmap.rods_pos[id] = SVector(model[id].pos...)
    end
    # reset force and torque vector, and threaded auxiliary arrays
    model.force_torque .= Ref(zero(ForceTorque))
    for ibatch in eachindex(model.clmap.output_threaded)
        model.clmap.output_threaded[ibatch] .= Ref(zero(ForceTorque))
    end
    if model.n_obs > 0
        ## update the cell list
        model.clmap.cell_list = CellListMap.UpdateCellList!(
            model.clmap.rods_pos,
            model.obs_pos,
            model.clmap.box,
            model.clmap.cell_list,
            model.clmap.aux,
        )
        # calculate and store pairwise forces and torques
        CellListMap.map_pairwise!(
            (x, y, i, j, d2, force_torque) -> calc_forces_torque!(x, y, i, model, force_torque),
            model.force_torque, model.clmap.box, model.clmap.cell_list;
            output_threaded=model.clmap.output_threaded
        )
    end
    # generate random numbers for agents
    model.ζ = rand(model.rng, model.agents.count)
end

# Helper functions
function unit_vec(x)
    x_mag = norm(x)
    if x_mag ≈ 0.0
        return (0.0, 0.0)
    else
        return x ./ x_mag
    end
end

function rad2vec(θ)
    return (cos(θ), sin(θ))
end

function rod_marker(a::ActiveRod)
    t = LinRange(-π / 2, π / 2, 100)
    x = cos.(t)
    y = sin.(t)
    n = rad2vec(a.θ)
    disk = permutedims([x y]) .* (a.d / 2)
    coords1 = [Point2f(x, y) for (x, y) in zip(disk[1, :], disk[2, :])] .+ Point2f(a.l / 2, 0.0)
    coords2 = [Point2f(-x, -y) for (x, y) in zip(disk[1, :], disk[2, :])] .- Point2f(a.l / 2, 0.0)
    coords = vcat(coords1, coords2)
    φ = a.θ
    scale(rotate2D(Polygon(coords), φ), 1)
end

function static_preplot!(ax, model)
    # plot obstacles
    if model.n_obs > 0
        poly!(Circle.(Point2f.(model.obs_pos[1:end]), repeat([model.obs_radius], size(model.obs_pos)[1])),
            color=:green)
    end
    hidedecorations!(ax)
end

function bench()
    model = initialize_model(n_rods=1000, n_obs=5000, d=1, l=2, seed=111, extent=(100.0, 100.0), dt=0.01, va=1.0, λ0=1e-4, k0=50.0)
    adf, _ = run!(model, agent_step!, model_step!, 1000;
        agents_first=false,
        adata=[:pos, :θ, :image], showprogress=false)
end

function onlysteps(nsteps=1000)
    model = initialize_model(n_rods=1000, n_obs=5000, d=1, l=2, seed=111, extent=(100.0, 100.0), dt=0.01, va=1.0, λ0=1e-4, k0=50.0)
    Agents.step!(
        model, agent_step!, model_step!, nsteps, false,
    )
end






